/**
 * @fecha 5/5/2021
 * @seccion Algoritmos y Estructuras de Datos 20 
 * @class JuntarUsuarios
*/ 

tempTopUsersRating = topUsersRating.groupby('videogameId').sum()[['similarityIndex','weightedRating']]
tempTopUsersRating.columns = ['sum_similarityIndex','sum_weightedRating']
tempTopUsersRating.head()