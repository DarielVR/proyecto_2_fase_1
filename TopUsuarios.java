/**
 * @fecha 5/5/2021
 * @seccion Algoritmos y Estructuras de Datos 20 
 * @class TopUsuarios
*/ 

topUsers=pearsonDF.sort_values(by='similarityIndex', ascending=False)[0:50]
topUsers.head()
