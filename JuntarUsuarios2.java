/**
 * @fecha 5/5/2021
 * @seccion Algoritmos y Estructuras de Datos 20 
 * @class JuntandoUsuarios2
*/ 

#Crea un nuevo dataframe
recommendation_df = pd.DataFrame()
#Ahora tomamos el promedio ponderado 
recommendation_df['weighted average recommendation score'] = tempTopUsersRating['sum_weightedRating']/tempTopUsersRating['sum_similarityIndex']
recommendation_df['videogameId'] = tempTopUsersRating.index
recommendation_df.head()