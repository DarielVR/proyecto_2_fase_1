/**
 * @fecha 5/5/2021
 * @seccion Algoritmos y Estructuras de Datos 20 
 * @class Import
*/ 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
movies_df = pd.read_csv('videogames.csv')
ratings_df = pd.read_csv('ratings.csv')