/**
 * @fecha 5/5/2021
 * @seccion Algoritmos y Estructuras de Datos 20 
 * @class ClasificacionUsuario
*/ 

topUsersRating=topUsers.merge(ratings_df, left_on='userId', right_on='userId', how='inner')
topUsersRating.head()